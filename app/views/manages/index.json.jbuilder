json.array!(@manages) do |manage|
  json.extract! manage, :id, :name, :String
  json.url manage_url(manage, format: :json)
end
